var express = require('express');
var app = express();

app.set("view engine", "ejs")
app.set("views", "./views")
app.use(express.static("public"))

var server = require("http").Server(app)
var io = require('socket.io')(server);
server.listen(process.env.PORT || 3000)

var config = {
    domain: "http://192.168.153.130:3000"
}


io.on("connection", function (socket) {
    console.log("co nguoi ket noi:" + socket.id)

    socket.on("send-text", function (data) {
        console.log("data:" + data)
        io.sockets.emit("server-text", data)
    })
    socket.on("disconnect", function () {
        console.log("co nguoi disconnect:" + socket.id)
    })
})

app.get("/", function (req, res, err) {
    res.render("trangchu", { config: config })
})